module ApplicationHelper
  def full_title(page_title)
    base_title = "BIGBAG Store"
    return base_title if page_title.empty?
    "#{page_title} - #{base_title}"
  end

  def active_dropdown(dropdown_name)
    dropdown_name == is_active_action_name ? "active" : nil
  end

  def is_active_action_name
    if params[:action] == "index"
      :home
    elsif params[:action].in?(
      [
        "product_grid_left_sidebar", "single_product", "cart_page", "checkout_step_1",
        "checkout_step_2", "checkout_step_3", "checkout_complete",
      ]
    )
      :shop
    elsif params[:action].in?(["about_us", "tokushoho", "privacy_policy"])
      :pages
    elsif params[:action].in?(
      [
        "blog_left_sidebar", "blog_right_sidebar",
        "blog_single_left_sidebar", "blog_single_right_sidebar",
      ]
    )
      :blog
    elsif params[:action] == "#"
      :account
    else
      nil
    end
  end

  def is_active_action(action_name)
    params[:action] == action_name ? "active" : nil
  end
end
